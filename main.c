#include <stdio.h>
#include <stdlib.h> 
#include "funcoesNeuronios.h"
#include <unistd.h>

int defineTamanhoEntrada();
int DefineNumeroDeImagens();
int AlocaEntradaP(NeuronioSt *neuronio, int tamanhoDaCamada);

int main(int argc, char *argv[]){
    int TamanhovetorEntrada = defineTamanhoEntrada();  //Entra dentro da matriz e define o tamanho dos vetores
    int Numero_de_imagens = DefineNumeroDeImagens(); //Entra dentro da matriz e define o numero de imagens diferentes que estao sendo processadas
    NeuronioSt *NeurAdress = NULL;  
     //Assim que conseguir esse valor sera alterado pelo tamnho do vetor daGLCM 
                        //vetorENtrada = sizeof(vetorGLCM)/GLCM;  
                        //                 (Vetor inteiro)/(Uma unidade)
    NeuronioSt **NCITG;//Neuronios de treinamento de 
   
    NCITG = (NeuronioSt**) malloc(Numero_de_imagens*sizeof(NeuronioSt*)); //Estou alocando '25 vetores de struct' dentro desses 25 vetores terei 536 structs    
    for(int i = 0 ; i < Numero_de_imagens ; i++)NCITG[i] = (NeuronioSt*) malloc(TamanhovetorEntrada*sizeof(NeuronioSt)); 

    FILE *trab2output;
    trab2output = fopen("trab2OutTreinamento.txt","r");
    char lixoChar; 
    double valor = 0;
   
    for(int i = 0 ; i < Numero_de_imagens ; i++){ //Os neuronio serao preparados a cada imagem
        AlocaEntradaP(NCITG[i],TamanhovetorEntrada); //puts("1");       
        for(int j = 0 ; j < TamanhovetorEntrada ; j++){ 
            fscanf(trab2output,"%lf",&valor);   
            fscanf(trab2output,"%c", &lixoChar);       
            for(int k = 0 ; k < TamanhovetorEntrada ; k++){ 
                NCITG[i][k].p[j] = valor;    
            } 
        }
        preparaOsNeuros(NCITG[i], TamanhovetorEntrada);
    }

    if(argv[1] == 0){
        puts("Por favor insira o numero de neuronios da camada oculta como parametro de entrada.\nSaindo...\n\n"); 
        return 0;     
    }
    int nNeurCamOcult = strtol(argv[1], NULL, 10);

    ///////////////////////////ALTERACAO DOS NEURONIOS DA CAMADA OCULTA 
    NeuronioSt **NCOG; 
    NCOG = (NeuronioSt**) malloc(Numero_de_imagens*sizeof(NeuronioSt*)); //Estou alocando '25 vetores de struct' dentro desses 25 vetores terei 536 structs    
    for(int i = 0 ; i < Numero_de_imagens ; i++)NCOG[i] = (NeuronioSt*) malloc(nNeurCamOcult*sizeof(NeuronioSt));

    //Esse trecho do codigo reutiliza as funcoes dos neuronios de entrada e altera e insere os valores dos neuronios da camada oculta. (:'
    for(int i = 0 ; i < Numero_de_imagens ; i++){ //Os neuronio serao preparados a cada imagem
        AlocaEntradaP(NCOG[i],nNeurCamOcult); //puts("1");
        for(int j = 0 ; j < nNeurCamOcult ; j++){ 
            for(int k = 0 ; k < TamanhovetorEntrada ; k++){ 
                NCOG[i][j].p[k] = NCITG[i][k].saida;
                //printf(" %lf",NCITG[i][k].saida );
            } 
        }
        preparaOsNeuros(NCOG[i], nNeurCamOcult);
    }
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////CAMADA DE SAIDA/////////////////////////
    NeuronioSt **NCFG; NCFG = (NeuronioSt**) malloc(Numero_de_imagens*sizeof(NeuronioSt));//Numero de neuronios da camada final
    for(int i = 0 ; i < Numero_de_imagens ; i++)NCFG[i] = (NeuronioSt*) malloc(sizeof(NeuronioSt));
    
    for(int i = 0 ; i < Numero_de_imagens ; i++){ 
        AlocaEntradaP(NCFG[i],nNeurCamOcult); //puts("1");
        for(int j = 0 ; j < nNeurCamOcult ; j++){ 
                NCFG[i][0].p[j] =  NCOG[i][j].saida;
        }
        preparaOsNeuros(NCFG[i],1);
    }

    for(int i = 0 ;i < 25 ; i++){
        printf("Saida do meu neuronio[%d] = %lf\n" ,i,  NCFG[i][0].saida); 
    }
    ////////////////////////////////////////////////////////////////////////

    return 0; 
}

int AlocaEntradaP(NeuronioSt *neuronio, int tamanhoDaCamada){
    NeuronioSt *neuronio_cam; neuronio_cam = neuronio;
    for(int i = 0 ; i < tamanhoDaCamada; i++){
        neuronio_cam[i].p = (double*) malloc(tamanhoDaCamada*sizeof(double));  
    }
}

int DefineNumeroDeImagens(){ //A partir do numero de linhas define o numero de imagens a serem abertas
    FILE *trab2output;
    
    trab2output = fopen("trab2OutTreinamento.txt","r");
    char ch;
    int nimagens = 0;  
    while((ch=fgetc(trab2output))!= EOF){
        if(ch == '\n')
        nimagens++;
    }
    
    //printf("%d\n", nimagens); 
 
    return nimagens;; 
}

int defineTamanhoEntrada(){
    FILE *trab2output;
    trab2output = fopen("trab2OutTreinamento.txt","r");
    char ch;
    int colunas = 0;  
    while((ch=fgetc(trab2output))!= EOF){
        if(ch == ';')
        colunas++;
        if(ch == '\n') 
        break; 
    }
    //printf("%d\n", colunas); 
    return colunas; 
}

// int nNeurCamOcult = strtol(argv[1], NULL, 10);
    // NeuronioSt **NCOTG; NCOTG = (NeuronioSt**) malloc(nNeurCamOcult*sizeof(NeuronioSt*));//Neuronios de treinamento camada oculta de grama
    //  for(int i = 0 ; i < nNeurCamOcult ; i++)NCOTG[i] = (NeuronioSt*) malloc(TamanhovetorEntrada*sizeof(NeuronioSt)); 

    // NeuronioSt **NCOTA; //Neuronios de treinamento camada oculta de Asfalto
    // puts("E apartir daqui o bagulho da..."); 
    
    //  for(int i = 0 ; i < Numero_de_imagens ; i++){ //Os neuronio serao preparados a cada imagem
    //      //printf("", i ); 
    //      preparaOsNeuros(NCOTG[i], nNeurCamOcult); //printf("%d", i);          
    //     for(int j = 0 ; j < nNeurCamOcult ; j ++){
    //          for(int k = 0 ; k < TamanhovetorEntrada ; k++){ 
    //             //NCOTG[i][j].p[k] = NCITG[i][k].saida;
    //             if(i == 0);
    //             //printf("%lf ",NCOTG[i][j].p[k]);   
    //         } 
    //     }
     //}
    
    
    //puts("Inacreditavel"); 

#include "Structsneuronios.h"
#include <time.h>
#include <math.h> 

///Comecar fazendo a linkagem. 
//  Proximo passo sera integrar a funcao Carrega meus neuronios na main e a partir de la 
//chamar todas as minhas funcoes de alocacao e etc. 
//Fazer um neuronio por vez... Fazer a chamada 536 vezes e a cada vez chamado adicionar um numero do vetor. 
  
int preparaOsNeuros(NeuronioSt *neuronio, int tamanhoDaCamada);
int *CarregaMeusNeuronios(NeuronioSt *neuronio, int tamanhoDaCamada);
int AlocaWneu(NeuronioSt *neuronio, int tamanhoDaCamada);
int geraWaleatorio(NeuronioSt *neuronio, int tamanhoDaCamada);
double *Somatorio(NeuronioSt *neuronio ,int tamanhoDaCamada);
void funcaoativacao(NeuronioSt *neuronio, int tamanhoDaCamada);
void normalizarSomatorio(NeuronioSt *neuronio, int tamanhoDaCamada);
int geraBaleatorio(NeuronioSt *neuronio, int tamanhoDaCamada);

int preparaOsNeuros(NeuronioSt *neuronio, int tamanhoDaCamada){ //Essa funcao chama as funcoes que relacionaodas aos neoronios    
    AlocaWneu(neuronio, tamanhoDaCamada); 
    geraWaleatorio(neuronio, tamanhoDaCamada); 
    geraBaleatorio(neuronio, tamanhoDaCamada); 
    Somatorio(neuronio, tamanhoDaCamada);  
    normalizarSomatorio(neuronio, tamanhoDaCamada);
    funcaoativacao(neuronio ,tamanhoDaCamada);  
}


int AlocaWneu(NeuronioSt *neuronio, int tamanhoDaCamada){
    NeuronioSt *neuronio_camWnum; neuronio_camWnum = neuronio;        
    for(int i = 0 ; i < tamanhoDaCamada; i++){
        neuronio_camWnum[i].w = (double*) malloc(tamanhoDaCamada*sizeof(double));  
    }
}


int geraWaleatorio(NeuronioSt *neuronio, int tamanhoDaCamada){
    int max = 1500, num; 
    NeuronioSt *myNeur; myNeur = neuronio;  
    srand(time(NULL)); 
    for(int i = 0 ; i < tamanhoDaCamada ; i++){
        for(int j = 0 ; j < tamanhoDaCamada ; j++){
            num = rand()%max + 1; 
            myNeur[i].w[j] = num;
            //printf("num = [%d]", num);
        } 
    }
    return 0; 
}

int geraBaleatorio(NeuronioSt *neuronio, int tamanhoDaCamada){
    int  max = 1500, num; 
    NeuronioSt *myNeur; myNeur = neuronio;  
    srand(time(NULL)); 
    for(int i = 0 ; i < tamanhoDaCamada ; i++){
        for(int j = 0 ; j < 536 ; j++){
            num = rand()%max + 1; 
            myNeur[i].b = num;
            //printf("B ale = [%d]", num);
        } 
    }
    return 0;
}

double *Somatorio(NeuronioSt *neuronio, int tamanhoDaCamada){
    NeuronioSt *myNeur; myNeur = neuronio;
      for(int i = 0 ; i < tamanhoDaCamada ; i++){
        for(int j = 0 ; j < 536 ; j++){
            myNeur[i].n += myNeur[i].p[j]*myNeur[i].w[j];
            //printf("%d ", j); 
        }
        myNeur[i].n += myNeur[i].b;  
        //printf("n = %lf\n\n",myNeur[i].n);
        //printf("%d", j); 

    }
    return 0; 
}

void normalizarSomatorio(NeuronioSt *neuronio, int tamanhoDaCamada){
    int menor = 0, maior = 0;
    NeuronioSt *myNeur; myNeur = neuronio;  
    for(int i = 0; i < tamanhoDaCamada; i++) {
        
        if (myNeur[i].n < menor){
            menor = myNeur[i].n;
        }
        else if (myNeur[i].n > maior) {
            maior = myNeur[i].n;
        }
    }
    for (int i = 0; i < tamanhoDaCamada; i++) {
        myNeur[i].n = (myNeur[i].n - menor) / (maior - menor);
    }
    return;
}

void funcaoativacao(NeuronioSt *neuronio, int tamanhoDaCamada){
    NeuronioSt *myNeur; myNeur = neuronio; 
    for(int j = 0 ; j < tamanhoDaCamada ; j++){
        //printf("%d ", myNeur[j].w);  //1/(1+)
        // printf("%lf ", myNeur[j].n); 
        myNeur[j].saida = 1/(1+ pow(2.718282,-myNeur[j].n));
        // printf("Valor signoide = %lf i = [%d]\n", myNeur[j].saida,j);
    }
    return; 
}
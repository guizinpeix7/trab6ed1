/* Universidade de Brasília - Faculdade do Gama
 * Estruturas de Dados 1 - 193704 TURMA: B
 * Professor: Mateus Mendelson
 * Nomes: Bruno Carmo Nunes 18/0117548
 *        Guilherme Dourado 16/0123020
 *        Guilherme Peixoto 17/0034941
 * Trabalho 2
*/

#ifndef MISC_H
#define MISC_H  
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

// Headers das funções  
float calcMediaMatrizVizinhanca(int *, int *, int, int);
void zerarTimeSeed();
unsigned int palavraBinariaParaDecimal(int *);
void normalizarVetor(double *);
void gerarMatrizImagem(int *, int *);
void gerarNomeArquivoAsfalto(char *);
void gerarNomeArquivoGrama(char *);
int rotacaoPalavraBinaria(int *);
void liberarMatriz(int *);
int gerarNumeroAleatorio(int , int );

double **aloca_matriz_glcm(double **);
double **GLCM(double **,double** ,int ,int );
double *summ_glcm(double**, double *);
int *co_ocurrence(double** ,double *);
double *energy_co_ocurrence(double**, double *);
double *contrast_co_ocurrence(double **, double *);
double *homogeneity_co_ocurrence(double **, double *);
double *vector_functions(double *,double *,double *, double *);
double *GLCM_main(int *);





float calcMediaMatrizVizinhanca(int *imagem, int *valoresMediaVizinhanca, int i, int j){
    int valor_1, valor_2, valor_3, valor_4, valor_5,valor_6, valor_7, valor_8, valor_9;
    float retorno;
        valor_1 = *(imagem + ((i-1)*colunas)+(j-1));
        valor_2 = *(imagem + ((i-1)*colunas)+j);
        valor_3 = *(imagem + ((i-1)*colunas)+(j+1));
        valor_4 = *(imagem + (i*colunas)+(j-1));
        valor_5 = *(imagem + (i*colunas)+j);
        valor_6 = *(imagem + (i*colunas)+(j+1));
        valor_7 = *(imagem + ((i+1)*colunas)+(j-1));
        valor_8 = *(imagem + ((i+1)*colunas)+j); 
        valor_9 = *(imagem + ((i+1)*colunas)+(j+1));
        int soma_matriz_vizinhanca_asfalto = valor_1 + valor_2 + valor_3 + valor_4 + valor_5 + valor_6 + valor_7 + valor_8 + valor_9;
        float media_ilbp = soma_matriz_vizinhanca_asfalto / 9;
        retorno = media_ilbp;
    
    
    for (int i = 0; i < 9; i++) {
        switch (i)
        {
        case 0:
            valoresMediaVizinhanca[i] = valor_1;
            break;
        
        case 1:
            valoresMediaVizinhanca[i] = valor_2;
            break;
        
        case 2:
            valoresMediaVizinhanca[i] = valor_3;
            break;
        
        case 3:
            valoresMediaVizinhanca[i] = valor_4;
            break;
        
        case 4:
            valoresMediaVizinhanca[i] = valor_5;
            break;

        case 5:
            valoresMediaVizinhanca[i] = valor_6;
            break;
        
        case 6:
            valoresMediaVizinhanca[i] = valor_7;
            break;
        
        case 7:
            valoresMediaVizinhanca[i] = valor_8;
            break;
        
        case 8:
            valoresMediaVizinhanca[i] = valor_9;
            break;

        default:
            break;
        }
         
    }

    return retorno;
}

void gerarNomeArquivoGrama(char *nome_arquivo){
    int indice_arquivo = rand() % 50 + 1;
    int novo_indice_arquivo = gerarNumeroAleatorio(indice_arquivo, 0);

    if(novo_indice_arquivo < 10){
        sprintf(nome_arquivo, "grass_0%d", novo_indice_arquivo);
        strcat(nome_arquivo, ".txt");
    }
    else{
        sprintf(nome_arquivo, "grass_%d", novo_indice_arquivo);
        strcat(nome_arquivo, ".txt");
    }
}

//Data: 23/04/2019
//Autor: Guilherme Dourado
//Função criada para gerar nome do arquivo de imagens de asfalto
void gerarNomeArquivoAsfalto(char *nome_arquivo){
    // printf("Vou gerar Nome do arquivo asfalto\n");
    int indice_arquivo = rand() % 50 + 1;
    int novo_indice_arquivo = gerarNumeroAleatorio(0,indice_arquivo);
    if(novo_indice_arquivo < 10){
        sprintf(nome_arquivo, "asphalt_0%d", novo_indice_arquivo);
        strcat(nome_arquivo, ".txt");
    }
    else{
        sprintf(nome_arquivo, "asphalt_%d", novo_indice_arquivo);
        strcat(nome_arquivo, ".txt");
    }
}

void zerarTimeSeed(){
    srand(time(NULL));
}

int gerarNumeroAleatorio(int indice_arquivo_asfalto, int indice_arquivo_grama) {
    FILE *arquivo_blacklist;
    int indice_arquivo;
    if(indice_arquivo_asfalto == 0)
    {
        arquivo_blacklist = fopen("blacklist-asfalto.txt", "a+");
        indice_arquivo = indice_arquivo_grama;
    }
    if(indice_arquivo_grama == 0)
    {
        arquivo_blacklist = fopen("blacklist-grama.txt", "a+");
        indice_arquivo = indice_arquivo_asfalto;
    }
    
    if(arquivo_blacklist != NULL){
        int numero_arquivo;
        int cont_igual = 0;
        rewind(arquivo_blacklist);
        while(!feof(arquivo_blacklist)){
            fscanf(arquivo_blacklist, "%d", &numero_arquivo);
            if(indice_arquivo == numero_arquivo){
                cont_igual++;
            }

        }

        if(cont_igual == 0){
            fclose(arquivo_blacklist);
            return indice_arquivo;
        }
        else if(indice_arquivo_asfalto != 0){
            indice_arquivo_asfalto = rand() % 50 + 1;
            indice_arquivo_grama = 0;
            gerarNumeroAleatorio(indice_arquivo_asfalto, indice_arquivo_grama);
        }
        else if(indice_arquivo_grama != 0){
            indice_arquivo_grama = rand() % 50 + 1;
            indice_arquivo_asfalto = 0;
            gerarNumeroAleatorio(indice_arquivo_asfalto, indice_arquivo_grama);
        }
        
    }
    else{
        printf("arquivo-blacklist.txt == null\n\n");
        indice_arquivo = indice_arquivo_asfalto + indice_arquivo_grama;
        return indice_arquivo;
    }
}

void liberarVetor(int *vetor){
    free(vetor);
}

int rotacaoPalavraBinaria(int *palavra_binaria){
    int numero_convertido_decimal, numero_convertido_decimal1, auxiliar_rotacao;
    //convertendo a palavra binária para um número decimal
    numero_convertido_decimal = palavraBinariaParaDecimal(palavra_binaria);
    
    //realizando todas as rotações possíveis dentro da palavra binária
    for(int k=0;k<9;k++){
        auxiliar_rotacao = palavra_binaria[0];
        palavra_binaria[0] = palavra_binaria[1];
        palavra_binaria[1] = palavra_binaria[2];
        palavra_binaria[2] = palavra_binaria[3];
        palavra_binaria[3] = palavra_binaria[4];
        palavra_binaria[4] = palavra_binaria[5];
        palavra_binaria[5] = palavra_binaria[6];
        palavra_binaria[6] = palavra_binaria[7];
        palavra_binaria[7] = palavra_binaria[8];
        palavra_binaria[8] = auxiliar_rotacao;

        numero_convertido_decimal1 = palavraBinariaParaDecimal(palavra_binaria);

        //checando qual é o menor valor gerado a partir da palavra binária
        if(numero_convertido_decimal > numero_convertido_decimal1) 
            numero_convertido_decimal = numero_convertido_decimal1;
    }
    if(numero_convertido_decimal > 511){
        printf("numero acima de 511: -> %d\n", numero_convertido_decimal);
    }
    //retorna menor numero decimal possível
    return numero_convertido_decimal;
}
unsigned int palavraBinariaParaDecimal(int *numero_palavra_binaria){

      unsigned int retorno = 0;

      if(numero_palavra_binaria[0] == 1) retorno += 256;
      if(numero_palavra_binaria[1] == 1) retorno += 128;
      if(numero_palavra_binaria[2] == 1) retorno += 64;
      if(numero_palavra_binaria[3] == 1) retorno += 32;
      if(numero_palavra_binaria[4] == 1) retorno += 16;
      if(numero_palavra_binaria[5] == 1) retorno += 8;
      if(numero_palavra_binaria[6] == 1) retorno += 4;
      if(numero_palavra_binaria[7] == 1) retorno += 2;
      if(numero_palavra_binaria[8] == 1) retorno += 1;

      return retorno;
}

void normalizarVetor(double *vetor){
    int menor = 0, maior = 0;

    for (int i = 0; i < 255; i++) {
        if (vetor[i] < menor){
            menor = vetor[i];
        }
        else if (vetor[i] > maior) {
            maior = vetor[i];
        }
    }

    for (int i = 0; i < 255; i++) {
        vetor[i] = (vetor[i] - menor) / (maior - menor);
    }
}

//Data: 23/04/2019
//Autor: Guilherme Dourado
//Função para gerar a Matriz com os valores de cada pixel da imagem
void gerarMatrizImagem(int *imagemAsfalto, int *imagemGrama){
    FILE *arquivo_asfalto, *arquivo_grama;

    zerarTimeSeed();

    char nome_arquivo_asfalto[14] = {}; 
    gerarNomeArquivoAsfalto(nome_arquivo_asfalto);
    arquivo_asfalto = fopen(nome_arquivo_asfalto, "r");

    if(arquivo_asfalto != NULL ){
       rewind(arquivo_asfalto);
        for(int i = 0; i < linhas; i++){
            for(int j = 0; j < colunas; j++){
                int valor_matriz_asfalto;
                fscanf(arquivo_asfalto, "%d;", &valor_matriz_asfalto);  
                *(imagemAsfalto + (i*colunas)+j)  = valor_matriz_asfalto;
                //printf("[%d] ", *(imagemAsfalto + (i*colunas)+j));
            }
           //  printf("\n"); 
        }
    }else {
        printf("Não pude abrir arquivo asfalto: %s\n", nome_arquivo_asfalto);
    }
    

    
//    for(int i = 0 ; i < 24 ; i++)
//        printf("%lf\n" , GLCM_ptr[i]);
   
    char nome_arquivo_grama[14] = {};    
    gerarNomeArquivoGrama(nome_arquivo_grama);
    arquivo_grama = fopen(nome_arquivo_grama, "r");

    if(arquivo_grama != NULL){
       rewind(arquivo_grama);
        for(int i = 0; i < linhas; i++){
            for(int j = 0; j < colunas; j++){
                int valor_matriz_grama;
                fscanf(arquivo_grama, "%d;", &valor_matriz_grama);
                *(imagemGrama + (i * colunas) + j) = valor_matriz_grama;
            }
        }
    }
    else{
        printf("não pude abrir arquivo grama: %s\n", nome_arquivo_grama);
    }
    
//    GLCM_main(imagemGrama); 
}

#endif

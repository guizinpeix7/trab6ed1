# EDA projeto 6

# Compilacao
Codigo para compilacao no linux.
clear && gcc -lm trab2.c && ./a.out && clear && gcc -lm main.c <br /> 
Para que o codigo seja executado no windows e necessario
1 - Compilar o trab2.c <br /> 
2 - Executar o trab2.c <br /> 
3 - Compilar a main.c <br /> 
4 - executar <br /> 
Ps: Sempre compilar usando a flag -lm <br />  

# Para Execucao 
./a.out<br />

Ps: o numero de neuronios da camada oculta pode ser passado como parametro junto como o codigo de execucao... Exemplo<br />
./a.out 520<br />

# Funcionamento<br />
A estrutura do codigo foi construida de forma satisfatoria e de forma dinamica.<br /> 
Grande parte das funcoes, como somatorio e ativacao dos neuronios, foram satisfatoriamente implementadas.<br />
O back propagation nao foi implementado.<br />
 
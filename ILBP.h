/* Universidade de Brasília - Faculdade do Gama
 * Estruturas de Dados 1 - 193704 TURMA: B
 * Professor: Mateus Mendelson
 * Nomes: Bruno Carmo Nunes 18/0117548
 *        Guilherme Dourado 16/0123020
 *        Guilherme Peixoto 17/0034941
 * Trabalho 2
*/

#ifndef TRAB2_H
#define TRAB2_H
#define linhas 1025
#define colunas 1025
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "misc.h"

//Função para gerar o vetor ILBP
void gerarVetorILBP(double *vetorILBPAsfalto, double *vetorILBPGrama){
    int **imagemAsfalto, **imagemGrama;

    imagemAsfalto = (int *)malloc(linhas*colunas*sizeof(int));
    imagemGrama = (int *)malloc(linhas*colunas*sizeof(int));

    if(imagemAsfalto != NULL && imagemGrama != NULL){
        gerarMatrizImagem((int *)imagemAsfalto, (int *)imagemGrama);

    //  for(int i = 0; i < linhas; i++){
    //     for(int j = 0; j < colunas; j++){
    //         printf("%d;", imagemAsfalto[i*j]); //Código de teste comentado

    //     }
    // }

    //  for(int i = 0; i < linhas; i++){
    //     for(int j = 0; j < colunas; j++){
    //         printf("%d;", imagemGrama[i*j]); //Código de teste comentado
            
    //     }
    // }
    
     
    //Zerando todas as posições d vetor ILBP
    for(int i = 0; i < 512; i++){
        *(vetorILBPGrama + i) = 0.0;
        *(vetorILBPAsfalto + i) = 0.0;
    }

    int palavra_binaria_asfalto[9], palavra_binaria_grama[9];
    float media_ilbp_asfalto = 0, media_ilbp_grama = 0;
    
    for(int i=1;i<=1023;i++){
        for(int j=1;j<=1023;j++){
            
            //Calculo  da média da Matriz vizinhança de cada pixel da imagem
            int valoresMediaVizinhanca[9];
            media_ilbp_asfalto = calcMediaMatrizVizinhanca((int *)imagemAsfalto, (int *)valoresMediaVizinhanca, i, j);
            //gerando palavra binaria
            for(int b=0;b<9;b++){
                if(valoresMediaVizinhanca[b] >= media_ilbp_asfalto){
                    palavra_binaria_asfalto[b] = 1;
                }else{
                    palavra_binaria_asfalto[b] = 0;
                }
               
            } 
            media_ilbp_grama = calcMediaMatrizVizinhanca((int *)imagemGrama, (int *)valoresMediaVizinhanca, i, j);
            for(int b=0;b<9;b++){
                if(valoresMediaVizinhanca[b] >= media_ilbp_grama){
                    palavra_binaria_grama[b] = 1;
                }else{
                    palavra_binaria_grama[b] = 0;
                }
                
            } 

            //incrementando os vetores ILBP na posição do menor valor que a palavra binária possui
            *(vetorILBPAsfalto + rotacaoPalavraBinaria(palavra_binaria_asfalto)) += 1;
            *(vetorILBPGrama + rotacaoPalavraBinaria(palavra_binaria_grama)) += 1;
            
            }
        }

    double *GLCM_asfalto;
    GLCM_asfalto = GLCM_main(imagemAsfalto);

    double *GLCM_grama;
    GLCM_grama = GLCM_main(imagemGrama);

   for(int i=512; i<536;i++){
      *(vetorILBPAsfalto + i) = GLCM_asfalto[(i-512)];
      *(vetorILBPGrama + i) = GLCM_grama[(i-512)]; 

   }

   
        //desalocanto vetores
        liberarVetor((int *)imagemAsfalto);
        liberarVetor((int *)imagemGrama);
    }
    
}

#endif

#ifndef STRUCTNEURONIOS 
#define STRUCTNEURONIOS
#include <stdio.h>
#include <stdlib.h>

struct NEURONIO_CAMADA_DE_ENTRADA{ //Para facilitar a vida veja o desenho 1
    double *p; //E o vetor recebido de alguma funcao o.O   
    double *w; //Vai fazer parte da funcao somatorio 
    double b; //Sera somado a funcao somatorio 
    double n; //Recabe os dados da funcao somatorio... Portanto nao e um array
    double saida; //Sera igual a funcao logistica de saida.
};
typedef struct NEURONIO_CAMADA_DE_ENTRADA NeuronioSt; 

 struct RESULTADOS{
    double testeGrama; 
    double testeAsfalto; 
};
typedef struct RESULTADOS resultados; 

// struct tipoHibrido{ Pode ser interessante usar um dia esse tipo hibrido de struct
//     camfin;          pra retorno de funcoes 'O. 
//     NeuronioSt;     
// };


#endif